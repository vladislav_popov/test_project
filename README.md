#Test project
## Requirements:
* Python 3.7
* Django 2.2.1
* psql 11.2

All requirements in requirements.txt

## Usage:
### Apply migrations:

`python manage.py migrate`

## Run tests:

`python manage.py test`

### Start bot:
`python manage.py test_bot`

Bot settings file in **settings/bot_settings.py**

### Frontend for viewing results:

`python manage.py runserver localhost:8000`

and then open **frontend/index.html**

 