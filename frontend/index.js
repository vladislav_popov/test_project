var users = new Vue({
        el: '#users',
        data: {
            users_list: [],
        },
        created() {
            this.loadData();
            setInterval(function () {
                this.loadData();
            }.bind(this), 30000);
        },
        methods: {
            loadData: function () {
                fetch('http://localhost:8000/api/users/')
                    .then((response) => {
                        if (response.ok) {
                            return response.json()
                        }
                        throw new Error('Network response was not ok');
                    })
                    .then((json) => {
                        this.users_list = [];
                        json.forEach(function (user) {
                            users.users_list.push({
                                email: user.email,
                                username: user.username
                            })
                        })
                    })
            },
        },
    })
;
var posts = new Vue({
        el: '#posts',
        data: {
            posts_list: [],
        },
        created() {
            this.loadData();
            setInterval(function () {
                this.loadData();
            }.bind(this), 30000);
        },
        methods: {
            loadData: function () {
                fetch('http://localhost:8000/api/posts/')
                    .then((response) => {
                        if (response.ok) {
                            return response.json()
                        }
                        throw new Error('Network response was not ok');
                    })
                    .then((json) => {
                        this.posts_list = [];
                        json.forEach(function (post) {
                            posts.posts_list.push({
                                rating: post.rating,
                                header: post.header,
                                body: post.body,
                            })
                        })
                    })
            },
        },
    })
;
var log_messages = new Vue({
        el: '#recent_activity',
        data: {
            log_messages_list: [],
        },
        created() {
            this.loadData();
            setInterval(function () {
                this.loadData();
            }.bind(this), 30000);
        },
        methods: {
            loadData: function () {
                fetch('http://localhost:8000/api/log_messages/')
                    .then((response) => {
                        if (response.ok) {
                            return response.json()
                        }
                        throw new Error('Network response was not ok');
                    })
                    .then((json) => {
                        this.log_messages_list = [];
                        json.forEach(function (log_message) {
                            log_messages.log_messages_list.push({
                                text: log_message.text,
                                datetime: log_message.datetime,
                            })
                        })
                    })
            },
        },
    })
;