from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    email = models.EmailField(unique=True)


class Post(models.Model):
    creator = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    header = models.TextField()
    body = models.TextField()
    rating = models.IntegerField(default=0)

    def __str__(self):
        return f"Post {self.header}, author: {self.creator}, likes: {self.rating}"


class LogMessage(models.Model):
    text = models.TextField()
    event_date = models.DateField(auto_now_add=True)
    event_time = models.TimeField(auto_now_add=True)