import factory

from test_project.models import User, Post


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User


class PostFactory(factory.django.DjangoModelFactory):
    creator = factory.SubFactory(UserFactory)

    class Meta:
        model = Post
