import httpretty
import json

from django.contrib.auth import get_user_model
from django.conf import settings
from rest_framework.reverse import reverse
from rest_framework.status import (is_success, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_201_CREATED,
                                   HTTP_202_ACCEPTED,
                                   HTTP_204_NO_CONTENT,
                                   HTTP_404_NOT_FOUND, HTTP_200_OK, HTTP_403_FORBIDDEN)
from rest_framework.test import APITestCase

from test_project.models import User, Post
from test_project.tests.factories import UserFactory, PostFactory


class TestProjectTestCase(APITestCase):
    create_user = True
    do_login = True

    def setUp(self) -> None:
        self.user_credentials = {
            "username": "username",
            "password": "password",
            "email": "123@123.ru"
        }

        if self.create_user:
            user_cls = get_user_model()
            self.user = user_cls.objects.create_user(**self.user_credentials)
        else:
            self.user = None

        if self.user and self.do_login:
            self.login_with_token(self.user_credentials)

    def login_with_token(self, credentials):
        response = self.client.post(path=reverse("token_obtain_pair"),
                                    data=credentials,
                                    format='json')
        token = json.loads(response.content.decode()).get("access")

        # setting obtained token to make client do authenticated calls:
        self.client.credentials(HTTP_AUTHORIZATION="Bearer %s" % token)


class CreatePostTestCase(TestProjectTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.user = UserFactory()

    def test_create_ok(self):
        resp = self.client.post(
            path=reverse('create_post'),
            format='json',
            data={
                'creator': self.user.id,
                'header': 'test header',
                'body': 'test body'
            }
        )
        self.assertEquals(resp.status_code, HTTP_201_CREATED)

        self.assertTrue(Post.objects.exists())
        post = Post.objects.get()

        self.assertEquals(post.creator, self.user)
        self.assertEquals(post.header, 'test header')
        self.assertEquals(post.body, 'test body')

    def test_not_authenticated(self):
        self.client.credentials(HTTP_AUTHORIZATION="")
        resp = self.client.post(
            path=reverse('create_post'),
            format='json',
            data={
                'creator': self.user.id,
                'header': 'test header',
                'body': 'test body'
            }
        )
        self.assertEquals(resp.status_code, HTTP_401_UNAUTHORIZED)


class TestPostChangeRatingTestCase(TestProjectTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.post = PostFactory()

    def test_like_ok(self):
        self.client.post(
            path=reverse('like_post'),
            format='json',
            data={
                'id': self.post.id,
            }
        )
        self.post.refresh_from_db()
        self.assertEquals(self.post.rating, 1)

    def test_dislike_ok(self):
        self.client.post(
            path=reverse('dislike_post'),
            format='json',
            data={
                'id': self.post.id,
            }
        )
        self.post.refresh_from_db()
        self.assertEquals(self.post.rating, -1)


class TestUserRegistrationTestCase(TestProjectTestCase):
    create_user = False
    do_login = False

    def test_register_ok(self):
        httpretty.enable()
        httpretty.register_uri(
            method=httpretty.GET,
            uri=f'{settings.EMAILHUNTER_BASE_URL}/email-verifier',
            body=json.dumps({
                'data': {
                    'score': settings.EMAILHUNTER_MIN_EMAIL_SCORE + 5
                }
            })
        )

        resp = self.client.post(
            path=reverse('register_user'),
            format='json',
            data={
                'username': 'user',
                'password': 'password',
                'email': 'test@test.ru'
            }
        )
        self.assertEquals(resp.status_code, HTTP_201_CREATED)
        expected_response = {
            'email': 'test@test.ru',
            'username': 'user'
        }
        self.assertEquals(json.loads(resp.content.decode()), expected_response)

        self.assertTrue(User.objects.exists())
        user = User.objects.get()
        self.assertEquals(user.username, 'user')
        self.assertEquals(user.email, 'test@test.ru')
        httpretty.disable()

    def test_not_unique_email(self):
        UserFactory(email='test@test.ru')

        httpretty.enable()
        httpretty.register_uri(
            method=httpretty.GET,
            uri=f'{settings.EMAILHUNTER_BASE_URL}/email-verifier',
            body=json.dumps({
                'data': {
                    'score': settings.EMAILHUNTER_MIN_EMAIL_SCORE + 5
                }
            })
        )

        resp = self.client.post(
            path=reverse('register_user'),
            format='json',
            data={
                'username': 'user',
                'password': 'password',
                'email': 'test@test.ru'
            }
        )
        self.assertEquals(resp.status_code, HTTP_400_BAD_REQUEST)

        expected_response = {
            'email': [
                "user with this email already exists."
            ]
        }
        self.assertEquals(json.loads(resp.content.decode()), expected_response)
        httpretty.disable()

    def test_bad_email(self):
        httpretty.enable()
        httpretty.register_uri(
            method=httpretty.GET,
            uri=f'{settings.EMAILHUNTER_BASE_URL}/email-verifier',
            body=json.dumps({
                'data': {
                    'score': settings.EMAILHUNTER_MIN_EMAIL_SCORE - 5
                }
            })
        )

        resp = self.client.post(
            path=reverse('register_user'),
            format='json',
            data={
                'username': 'user',
                'password': 'password',
                'email': 'test@test.ru'
            }
        )

        self.assertEquals(resp.status_code, HTTP_400_BAD_REQUEST)
        expected_response = {
            'email': [
                "Please enter correct email"
            ]
        }
        self.assertEquals(json.loads(resp.content.decode()), expected_response)
        httpretty.disable()
