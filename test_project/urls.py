"""test_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from test_project.api import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/create_post/', views.PostCreateView.as_view(), name='create_post'),
    path('api/like_post/', views.PostLikeView.as_view(), name='like_post'),
    path('api/dislike_post/', views.PostDislikeView.as_view(), name='dislike_post'),
    path('api/register/', views.UserRegisterView.as_view(), name='register_user'),
    path('api/users/', views.UserListView.as_view(), name='list_user'),
    path('api/posts/', views.PostListView.as_view(), name='list_post'),
    path('api/log_messages/', views.LogMessageListView.as_view(), name='list_log_messages'),
]

