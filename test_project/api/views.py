from django.db.models import F
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from test_project.models import Post, User, LogMessage
from test_project.api.serializer import PostCreateSerializer, PostChangeRatingSerializer, UserRegisterSerializer, LogMessageSerializer


class UserRegisterView(generics.CreateAPIView):
    serializer_class = UserRegisterSerializer


class UserListView(generics.ListAPIView):
    serializer_class = UserRegisterSerializer
    queryset = User.objects.all()


class PostCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PostCreateSerializer


class PostListView(generics.ListAPIView):
    serializer_class = PostCreateSerializer
    queryset = Post.objects.all()


class ChangePostRatingView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PostChangeRatingSerializer

    def get_object(self):
        id = self.request.data.get('id')
        obj = get_object_or_404(Post, id=id)
        return obj

    def perform_create(self, serializer):
        raise NotImplementedError


class PostLikeView(ChangePostRatingView):
    def perform_create(self, serializer):
        post = self.get_object()
        post.rating = F('rating') + 1
        post.save(update_fields=["rating"])


class PostDislikeView(ChangePostRatingView):
    def perform_create(self, serializer):
        post = self.get_object()
        post.rating = F('rating') - 1
        post.save(update_fields=["rating"])


class LogMessageListView(generics.ListAPIView):
    queryset = LogMessage.objects.all()
    serializer_class = LogMessageSerializer