import requests

from django.conf import settings
from rest_framework import serializers

from test_project.models import Post, User, LogMessage


class PostCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('id', 'creator', 'header', 'body', 'rating')
        read_only_fields = ('rating',)


class PostChangeRatingSerializer(serializers.Serializer):
    id = serializers.IntegerField()


class UserRegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        style={'input_type': 'password'}
    )

    class Meta:
        model = User
        fields = ('username', 'password', 'email')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

    def validate_email(self, value):
        payload = {
            'api_key': settings.EMAILHUNTER_API_KEY,
            'email': value
        }
        r = requests.get(f'{settings.EMAILHUNTER_BASE_URL}/email-verifier', params=payload)
        if r.ok and r.json()['data']['score'] > settings.EMAILHUNTER_MIN_EMAIL_SCORE:
            return value
        else:
            raise serializers.ValidationError("Please enter correct email")

    def to_representation(self, instance):
        return {
            'email': instance.email,
            'username': instance.username,
        }


class LogMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogMessage
        fields = ('text', )

    def to_representation(self, instance):
        return {
            'text': instance.text,
            'datetime': f'{instance.event_date} {instance.event_time}',
        }