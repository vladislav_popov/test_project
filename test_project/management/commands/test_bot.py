import json
import random

from django.core.management import BaseCommand
from django.db.models import Count
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from test_project.models import *
from settings.bot_settings import BOT_SETTINGS

client = APIClient()


class Command(BaseCommand):
    def user_login(self, user: User):
        resp = client.post(reverse('token_obtain_pair'), {'username': user.username, 'password': 'password'})
        token = json.loads(resp.content.decode()).get("access")
        print(token)
        client.credentials(HTTP_AUTHORIZATION="Bearer %s" % token)

    def user_logout(self):
        client.credentials(HTTP_AUTHORIZATION="")

    def create_users(self):
        for i in range(BOT_SETTINGS['number_of_users']):
            data = {
                'username': f'user{i}',
                'password': 'password',
                'email': f'test{i}@test.ru'
            }
            client.post(reverse('register_user'), data, format='json')
            LogMessage.objects.create(text=f'User{i} was created')

    def create_posts(self):
        for user in User.objects.all():
            self.user_login(user)
            for i in range(random.randint(1, BOT_SETTINGS['max_posts_per_user'])):
                header = f'{user.username}\'s post #{i}'
                data = {
                    'creator': user.id,
                    'header': header,
                    'body': 'post body'
                }
                client.post(reverse('create_post'), data, format='json')
                LogMessage.objects.create(text=f'{user.username} created a post {header}')
            self.user_logout()

    def get_random_post(self) -> Post:
        count = Post.objects.aggregate(count=Count('id'))['count']
        random_index = random.randint(0, count - 1)
        return Post.objects.all()[random_index]

    def like_posts(self):
        for user in User.objects.all():
            self.user_login(user)
            for i in range(random.randint(1, BOT_SETTINGS['max_likes_per_user'])):
                post = self.get_random_post()
                client.post(reverse('like_post'), {'id': post.id}, format='json')
                LogMessage.objects.create(text=f'{user.username} liked post: {post.header}')
            self.user_logout()

    def handle(self, *args, **options):
        self.create_users()
        self.create_posts()
        self.like_posts()
